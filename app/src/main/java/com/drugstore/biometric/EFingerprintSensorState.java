package com.drugstore.biometric;

public enum EFingerprintSensorState {
    NOT_SUPPORTED,
    NOT_BLOCKED,
    NO_FINGERPRINTS,
    READY
}
