package com.drugstore.biometric;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.hardware.fingerprint.FingerprintManagerCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import com.drugstore.R;

public class FingerprintDialogFragment extends DialogFragment
        implements FingerprintHelper.Callback {

    private FingerprintManagerCompat.CryptoObject mCryptoObject;
    private FingerprintHelper mFingerprintHelper;
    private boolean isRealAccount;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Material_Light_Dialog);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle(getString(R.string.fingerprint_dialog_title));
        View view = inflater.inflate(R.layout.dialog_fingerprint, container, false);

        TextView fpDialogDescription = view.findViewById(R.id.fingerprint_dialog_description);
        fpDialogDescription.setText(getString(R.string.fingerprint_dialog_description));

        ImageView fpDialogIcon = view.findViewById(R.id.fingerprint_dialog_icon);

        TextView fpDialogStatus = view.findViewById(R.id.fingerprint_dialog_status);
        fpDialogStatus.setHint(getString(R.string.fingerprint_dialog_hint));

        Button fpDialogCancelButton = view.findViewById(R.id.fingerprint_dialog_cancel_button);
        fpDialogCancelButton.setText(getString(R.string.fingerprint_dialog_cancel));
        fpDialogCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        mFingerprintHelper = new FingerprintHelper(getActivity(), fpDialogIcon, fpDialogStatus, this);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mFingerprintHelper.startAuth(mCryptoObject);
    }

    @Override
    public void onPause() {
        if (mFingerprintHelper != null) {
            mFingerprintHelper.cancel();
        }
        dismissAllowingStateLoss();
        super.onPause();
    }

    public void setCryptoObject(FingerprintManagerCompat.CryptoObject cryptoObject) {
        this.mCryptoObject = cryptoObject;
    }

    public void setAccountType(boolean isRealAccount) {
        this.isRealAccount = isRealAccount;
    }

    @Override
    public void show(@NonNull FragmentManager manager, @Nullable String tag) {
        FragmentTransaction ft = manager.beginTransaction();
        ft.add(this, tag);
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onAuthenticated(String password) {
        Intent intent = new Intent();
        intent.putExtra(FingerprintUtils.PASSWORD_BUNDLE_KEY, password);
        intent.putExtra(FingerprintUtils.ACCOUNT_TYPE_BUNDLE_KEY, isRealAccount);
        getTargetFragment().onActivityResult(getTargetRequestCode(),
                FingerprintUtils.FINGERPRINT_REQUEST_CODE_OK, intent);
    }

    @Override
    public void onError() {
        onDismiss();
    }

    @Override
    public void onDismiss() {
        if (isAdded()) {
            dismissAllowingStateLoss();
        }
    }
}
