package com.drugstore.biometric;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.core.hardware.fingerprint.FingerprintManagerCompat;
import androidx.core.os.CancellationSignal;
import com.drugstore.R;

import javax.crypto.Cipher;

@TargetApi(Build.VERSION_CODES.M)
public class FingerprintHelper extends FingerprintManagerCompat.AuthenticationCallback {

    private static final String NAME = "PreferencesOfApplication";
    private static final String FIELD_PASSWORD = "password";

    private static final long ERROR_TIMEOUT_MILLIS = 2000;
    private static final long SUCCESS_DELAY_MILLIS = 1300;

    private final ImageView mIcon;
    private final TextView mErrorTextView;

    private CancellationSignal mCancellationSignal;
    private Callback mCallback;
    private Context mContext;

    private boolean mSelfCancelled;

    public FingerprintHelper(Context context, ImageView icon, TextView errorTextView, Callback callback) {
        mContext = context;
        mIcon = icon;
        mErrorTextView = errorTextView;
        mCallback = callback;
    }

    public interface Callback {
        void onAuthenticated(String password);

        void onError();

        void onDismiss();
    }

    public void startAuth(FingerprintManagerCompat.CryptoObject cryptoObject) {
        mCancellationSignal = new CancellationSignal();
        mSelfCancelled = false;
        FingerprintManagerCompat manager = FingerprintManagerCompat.from(mContext);
        manager.authenticate(cryptoObject, 0, mCancellationSignal, this, null);
    }

    public void cancel() {
        if (mCancellationSignal != null) {
            mSelfCancelled = true;
            mCancellationSignal.cancel();
            mCancellationSignal = null;
        }
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManagerCompat.AuthenticationResult result) {
        // все прошло успешно
        Cipher cipher = result.getCryptoObject().getCipher();
        SharedPreferences sPreference = mContext.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        String encoded = sPreference.getString(FIELD_PASSWORD, "");
        String decoded = CryptoUtils.decode(encoded, cipher);

        mErrorTextView.removeCallbacks(mResetErrorTextRunnable);
        mIcon.setImageResource(R.drawable.ic_fingerprint_success);
        mErrorTextView.setTextColor(mErrorTextView.getResources()
                .getColor(R.color.fingerprint_dialog_success_color, null));
        mErrorTextView.setText(mContext.getString(R.string.fingerprint_dialog_success));
        mCallback.onAuthenticated(decoded);
        mIcon.postDelayed(new Runnable() {
            @Override
            public void run() {
                mCallback.onDismiss();
            }
        }, SUCCESS_DELAY_MILLIS);
    }

    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        // грязные пальчики, недостаточно сильный зажим
        showError(helpString);
    }

    @Override
    public void onAuthenticationFailed() {
        // отпечаток считался, но не распознался
        showError(mContext.getString(R.string.fingerprint_dialog_not_recognized));
    }

    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        // несколько неудачных попыток считывания (5)
        // после этого сенсор станет недоступным на некоторое время (30 сек)
        if (!mSelfCancelled) {
            showError(errString);
            mIcon.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mCallback.onError();
                }
            }, ERROR_TIMEOUT_MILLIS);
        }
    }

    private void showError(CharSequence error) {
        mIcon.setImageResource(R.drawable.ic_fingerprint_error);
        mErrorTextView.setText(error);
        mErrorTextView.setTextColor(mErrorTextView.getResources()
                .getColor(R.color.fingerprint_dialog_warning_color, null));
        mErrorTextView.removeCallbacks(mResetErrorTextRunnable);
        mErrorTextView.postDelayed(mResetErrorTextRunnable, ERROR_TIMEOUT_MILLIS);
    }

    private Runnable mResetErrorTextRunnable = new Runnable() {
        @Override
        public void run() {
            mErrorTextView.setTextColor(mErrorTextView.getResources()
                    .getColor(R.color.fingerprint_dialog_hint_color, null));
            mErrorTextView.setText(mContext.getString(R.string.fingerprint_dialog_hint));
            mIcon.setImageResource(R.drawable.ic_fingerprint);
        }
    };
}
