package com.drugstore.biometric;

import android.app.KeyguardManager;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.core.hardware.fingerprint.FingerprintManagerCompat;

public final class FingerprintUtils {
    public static final int FINGERPRINT_REQUEST_CODE_OK = 158741;
    public static final String PASSWORD_BUNDLE_KEY = "password_key";
    public static final String ACCOUNT_TYPE_BUNDLE_KEY = "account_type_key";
    public static final String FINGERPRINT_DIALOG_FRAGMENT_TAG = "fingerprintFragment";

    private FingerprintUtils() {
    }

    public static boolean isSensorStateAt(@NonNull EFingerprintSensorState state, @NonNull Context context) {
        return checkSensorState(context) == state;
    }

    private static boolean checkFingerprintCompatibility(@NonNull Context context) {
        return FingerprintManagerCompat.from(context).isHardwareDetected();
    }

    private static EFingerprintSensorState checkSensorState(@NonNull Context context) {
        if (checkFingerprintCompatibility(context)) {
            KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
            if (!keyguardManager.isKeyguardSecure()) {
                return EFingerprintSensorState.NOT_BLOCKED;
            }
            if (!FingerprintManagerCompat.from(context).hasEnrolledFingerprints()) {
                return EFingerprintSensorState.NO_FINGERPRINTS;
            }
            return EFingerprintSensorState.READY;
        } else {
            return EFingerprintSensorState.NOT_SUPPORTED;
        }
    }
}
