package com.drugstore.core

import android.content.Context
import com.drugstore.R
import com.drugstore.entity.Gender
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.util.*

private val DATE_1_FORMATTER: DateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")
fun date1(date: LocalDate?): String? {
    return date?.let { DATE_1_FORMATTER.format(it) }
}

private val GENDER_TO_NAME_RES_ID_MAP = EnumMap<Gender, Int>(Gender::class.java).apply {
    for (gender in Gender.values()) {
        this[gender] = when (gender) {
            Gender.MALE -> R.string.gender_male
            Gender.FEMALE -> R.string.gender_female
        }
    }
}
fun genderName(context: Context, gender: Gender?): String? {
    return GENDER_TO_NAME_RES_ID_MAP[gender]?.let { context.getString(it) }
}