package com.drugstore.core

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

// TODO set proper placeholder
@BindingAdapter("android:bind_imageUri")
fun setImageUri(view: ImageView, imageUri: String?) {
    Glide.with(view.context)
        .load(imageUri)
        .into(view)
}