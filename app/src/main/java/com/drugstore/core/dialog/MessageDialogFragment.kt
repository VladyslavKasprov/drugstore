package com.drugstore.core.dialog

import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.navArgs
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class MessageDialogFragment : DialogFragment() {

    private val args: MessageDialogFragmentArgs by navArgs()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val message = if (args.messageResId != 0) getString(args.messageResId) else args.message

        return MaterialAlertDialogBuilder(requireContext())
            .setTitle(args.titleResId)
            .setMessage(message)
            .setPositiveButton(args.buttonTextResId, null)
            .create()
    }
}