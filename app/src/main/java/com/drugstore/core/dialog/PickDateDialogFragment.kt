package com.drugstore.core.dialog

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.view.ContextThemeWrapper
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.navArgs
import com.android.core.fragment.navigateUp
import com.drugstore.R
import org.threeten.bp.LocalDate

class PickDateDialogFragment : DialogFragment() {

    private val args: PickDateDialogFragmentArgs by navArgs()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return DatePickerDialog(
            ContextThemeWrapper(requireContext(), R.style.ThemeOverlay_Dialog_DatePicker),
            { _, year, month, dayOfMonth -> onDatePicked(LocalDate.of(year, month + 1, dayOfMonth)) },
            args.initialDate.year, args.initialDate.monthValue - 1, args.initialDate.dayOfMonth
        )
    }

    internal fun onDatePicked(date: LocalDate) {
        val data = Intent().putExtras(bundleOf(PICK_DATE_PICKED_KEY to date))
        fragmentManager?.primaryNavigationFragment?.onActivityResult(args.requestCode, Activity.RESULT_OK, data)
        navigateUp()
    }
}

const val PICK_DATE_PICKED_KEY = "PICK_DATE_PICKED_KEY"