package com.drugstore.core.dialog

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.navArgs
import com.android.core.fragment.navigateUp
import com.drugstore.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.util.*

class PickItemDialogFragment : DialogFragment() {

    private val args: PickItemDialogFragmentArgs by navArgs()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val title = if (args.titleResId != 0) getString(args.titleResId) else args.title
        val checkedPosition = Arrays.binarySearch(args.ids, args.checkedItemId)

        return MaterialAlertDialogBuilder(requireContext())
            .setTitle(title)
            .setSingleChoiceItems(args.labels, checkedPosition) { _, position -> onItemPicked(args.ids[position]) }
            .setPositiveButton(android.R.string.ok) { _, _ -> onItemPicked(args.checkedItemId) }
            .setNegativeButton(R.string.all_cancel, null)
            .create()
    }

    internal fun onItemPicked(id: Int) {
        val data = Intent().putExtras(bundleOf(PICK_ITEM_INT_RESULT_KEY to id))
        fragmentManager?.primaryNavigationFragment?.onActivityResult(args.requestCode, Activity.RESULT_OK, data)
        navigateUp()
    }
}

const val PICK_ITEM_INT_RESULT_KEY = "PICK_ITEM_INT_RESULT_KEY"