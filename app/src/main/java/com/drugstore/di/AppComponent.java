package com.drugstore.di;

import android.app.Application;
import com.drugstore.App;
import com.drugstore.di.activity.ActivityModule;
import com.drugstore.di.viewmodel.ViewModelModule;
import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;

import javax.inject.Singleton;

@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        ActivityModule.class,
        ViewModelModule.class,
        RepositoryModule.class
})
public interface AppComponent extends AndroidInjector<App> {

    @Component.Factory
    interface Factory {
        AppComponent create(@BindsInstance Application application);
    }
}