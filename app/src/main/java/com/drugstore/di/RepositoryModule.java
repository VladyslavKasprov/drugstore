package com.drugstore.di;

import android.app.Application;
import com.drugstore.repository.di.DaggerRepositoryComponent;
import com.drugstore.repository.di.RepositoryComponent;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

@Module
abstract class RepositoryModule {

    @Provides
    @Singleton
    static RepositoryComponent appDatabase(Application application) {
        return DaggerRepositoryComponent.factory().create(application);
    }
}