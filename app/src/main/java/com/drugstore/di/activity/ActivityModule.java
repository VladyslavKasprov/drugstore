package com.drugstore.di.activity;

import com.drugstore.login.LoginActivity;
import com.drugstore.main.MainActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {

    @ContributesAndroidInjector(modules = MainActivityModule.class)
    @ActivitySingleton
    abstract MainActivity mainActivity();

    @ContributesAndroidInjector(modules = LoginActivityModule.class)
    @ActivitySingleton
    abstract LoginActivity loginActivity();
}