package com.drugstore.di.activity;

import com.drugstore.login.login.LoginFragment;
import com.drugstore.login.login.forgotpassword.ForgotPasswordFragment;
import com.drugstore.login.login.register.RegisterFragment;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
abstract class LoginActivityModule {

    @ContributesAndroidInjector
    abstract LoginFragment loginFragment();

    @ContributesAndroidInjector
    abstract ForgotPasswordFragment forgotPasswordFragment();

    @ContributesAndroidInjector
    abstract RegisterFragment registerFragment();
}