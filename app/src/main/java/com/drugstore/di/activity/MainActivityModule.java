package com.drugstore.di.activity;

import com.drugstore.main.categories.CategoriesFragment;
import com.drugstore.main.categories.products.ProductsFragment;
import com.drugstore.main.categories.products.product.ProductFragment;
import com.drugstore.main.contactus.ContactUsFragment;
import com.drugstore.main.orderhistory.OrderHistoryFragment;
import com.drugstore.main.privacypolicy.PrivacyPolicyFragment;
import com.drugstore.main.profile.ProfileFragment;
import com.drugstore.main.profile.addresses.AddressesFragment;
import com.drugstore.main.profile.addresses.persist.PersistAddressFragment;
import com.drugstore.main.profile.changepassword.ChangePasswordFragment;
import com.drugstore.main.profile.editprofile.EditProfileFragment;
import com.drugstore.main.settings.SettingsFragment;
import com.drugstore.main.settings.languages.LanguagesFragment;
import com.drugstore.main.settings.notifications.NotificationsFragment;
import com.drugstore.main.termsandconditions.TermsAndConditionsFragment;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
abstract class MainActivityModule {

    @ContributesAndroidInjector
    abstract CategoriesFragment categoriesFragment();

    @ContributesAndroidInjector
    abstract ProductsFragment productsFragment();

    @ContributesAndroidInjector
    abstract ProductFragment productFragment();


    @ContributesAndroidInjector
    abstract OrderHistoryFragment orderHistoryFragment();


    @ContributesAndroidInjector
    abstract ProfileFragment profileFragment();

    @ContributesAndroidInjector
    abstract EditProfileFragment editProfileFragment();

    @ContributesAndroidInjector
    abstract AddressesFragment addressesFragment();

    @ContributesAndroidInjector
    abstract PersistAddressFragment persistAddressFragment();

    @ContributesAndroidInjector
    abstract ChangePasswordFragment changePasswordFragment();


    @ContributesAndroidInjector
    abstract SettingsFragment settingsFragment();

    @ContributesAndroidInjector
    abstract NotificationsFragment notificationsFragment();

    @ContributesAndroidInjector
    abstract LanguagesFragment languagesFragment();


    @ContributesAndroidInjector
    abstract TermsAndConditionsFragment termsAndConditionsFragment();


    @ContributesAndroidInjector
    abstract PrivacyPolicyFragment privacyPolicyFragment();


    @ContributesAndroidInjector
    abstract ContactUsFragment contactUsFragment();
}