package com.drugstore.di.viewmodel;

import androidx.lifecycle.ViewModel;
import dagger.MapKey;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Target(METHOD)
@Retention(RUNTIME)
@MapKey
@interface ViewModelKey {
    Class<? extends ViewModel> value();
}