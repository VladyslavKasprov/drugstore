package com.drugstore.di.viewmodel;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.drugstore.login.login.LoginVM;
import com.drugstore.login.login.forgotpassword.ForgotPasswordVM;
import com.drugstore.login.login.register.RegisterVM;
import com.drugstore.main.MainActivityVM;
import com.drugstore.main.categories.CategoriesVM;
import com.drugstore.main.categories.products.ProductsVM;
import com.drugstore.main.categories.products.product.ProductVM;
import com.drugstore.main.contactus.ContactUsVM;
import com.drugstore.main.orderhistory.OrderHistoryVM;
import com.drugstore.main.profile.ProfileVM;
import com.drugstore.main.profile.addresses.AddressesVM;
import com.drugstore.main.profile.addresses.persist.PersistAddressVM;
import com.drugstore.main.profile.changepassword.ChangePasswordVM;
import com.drugstore.main.profile.editprofile.EditProfileVM;
import com.drugstore.main.settings.SettingsVM;
import com.drugstore.main.settings.languages.LanguagesVM;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds @ViewModelKey(MainActivityVM.class) @IntoMap
    abstract ViewModel mainActivityVM(MainActivityVM model);


    @Binds @ViewModelKey(CategoriesVM.class) @IntoMap
    abstract ViewModel categoriesVM(CategoriesVM model);

    @Binds @ViewModelKey(ProductsVM.class) @IntoMap
    abstract ViewModel productsVM(ProductsVM model);

    @Binds @ViewModelKey(ProductVM.class) @IntoMap
    abstract ViewModel productVM(ProductVM model);


    @Binds @ViewModelKey(OrderHistoryVM.class) @IntoMap
    abstract ViewModel orderHistoryVM(OrderHistoryVM model);


    @Binds @ViewModelKey(ProfileVM.class) @IntoMap
    abstract ViewModel profileVM(ProfileVM model);

    @Binds @ViewModelKey(EditProfileVM.class) @IntoMap
    abstract ViewModel editProfileVM(EditProfileVM model);

    @Binds @ViewModelKey(AddressesVM.class) @IntoMap
    abstract ViewModel addressesVM(AddressesVM model);

    @Binds @ViewModelKey(PersistAddressVM.class) @IntoMap
    abstract ViewModel persistAddressVM(PersistAddressVM model);

    @Binds @ViewModelKey(ChangePasswordVM.class) @IntoMap
    abstract ViewModel changePasswordVM(ChangePasswordVM model);


    @Binds @ViewModelKey(SettingsVM.class) @IntoMap
    abstract ViewModel settingsVM(SettingsVM model);

    @Binds @ViewModelKey(LanguagesVM.class) @IntoMap
    abstract ViewModel languagesVM(LanguagesVM model);


    @Binds @ViewModelKey(ContactUsVM.class) @IntoMap
    abstract ViewModel contactUsVM(ContactUsVM model);


    @Binds @ViewModelKey(LoginVM.class) @IntoMap
    abstract ViewModel loginVM(LoginVM model);

    @Binds @ViewModelKey(ForgotPasswordVM.class) @IntoMap
    abstract ViewModel forgotPasswordVM(ForgotPasswordVM model);

    @Binds @ViewModelKey(RegisterVM.class) @IntoMap
    abstract ViewModel registerVM(RegisterVM model);


    @Binds
    abstract ViewModelProvider.Factory viewModelFactory(ViewModelFactory factory);
}