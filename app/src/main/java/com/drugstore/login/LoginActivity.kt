package com.drugstore.login

import com.android.core.activity.BaseActivity
import com.android.core.config.ContentView
import com.android.core.config.activity.NavHostFragment
import com.android.core.config.activity.Theme
import com.android.core.config.activity.Toolbar
import com.drugstore.R
import com.drugstore.databinding.ActivityLoginBinding

@Theme(R.style.Theme)
@ContentView(R.layout.activity_login)
@NavHostFragment(R.id.fragment_login_nav_host)
@Toolbar(R.id.tb_login)
class LoginActivity : BaseActivity<ActivityLoginBinding>()