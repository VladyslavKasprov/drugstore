package com.drugstore.login.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.android.core.config.ContentView
import com.android.core.config.fragment.*
import com.android.core.fragment.BaseFragment
import com.android.core.fragment.localViewModels
import com.android.core.fragment.navigateTo
import com.android.core.fragment.observeEvent
import com.drugstore.BR
import com.drugstore.R
import com.drugstore.databinding.FragmentLoginBinding
import com.drugstore.main.MainActivity

@ContentView(R.layout.fragment_login)
@TopAppBarBackground(0)
@TopAppBarElevation(0f)
@ShouldInsetTopAppBar(false)
@LightStatusBar(false)
@LightNavigationBar(false)
class LoginFragment : BaseFragment<FragmentLoginBinding>() {

    internal val model: LoginVM by localViewModels()

    override fun onViewCreated(_view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(_view, savedInstanceState)
        bind(BR.model, model)

        observeEvent(model.onNavigateTo) { navigateTo(it) }

        view?.btnLoginLogin?.setOnClickListener { navigateToMainActivity() }
    }

    internal fun navigateToMainActivity() {
        val intent = Intent(requireActivity(), MainActivity::class.java)
        val bundleExtra = requireActivity().intent.getBundleExtra("deepLinkExtras")
        bundleExtra?.let { intent.putExtras(it) }
        startActivity(intent)
        requireActivity().finish()
    }
}