package com.drugstore.login.login

import com.android.core.lifecycle.BaseViewModel
import com.android.core.lifecycle.set
import javax.inject.Inject

internal class LoginVM @Inject internal constructor() : BaseViewModel() {

    fun forgotPassword() {
        navigateTo.set(LoginFragmentDirections.actionLoginToForgotPassword())
    }

    fun register() {
        navigateTo.set(LoginFragmentDirections.actionLoginToRegister())
    }
}