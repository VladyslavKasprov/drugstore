package com.drugstore.login.login.forgotpassword

import android.os.Bundle
import com.android.core.config.ContentView
import com.android.core.fragment.*
import com.drugstore.BR
import com.drugstore.R
import com.drugstore.databinding.FragmentForgotPasswordBinding

@ContentView(R.layout.fragment_forgot_password)
class ForgotPasswordFragment : BaseFragment<FragmentForgotPasswordBinding>() {

    internal val model: ForgotPasswordVM by localViewModels()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        bind(BR.model, model)

        observeEvent(model.onNavigateTo) { navigateTo(it) }
        observeEvent(model.onNavigateUp) { navigateUp() }
    }
}