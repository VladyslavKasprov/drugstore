package com.drugstore.login.login.forgotpassword

import com.android.core.lifecycle.BaseViewModel
import com.android.core.lifecycle.call
import com.android.core.lifecycle.set
import com.drugstore.R
import javax.inject.Inject

class ForgotPasswordVM @Inject internal constructor() : BaseViewModel() {

    fun recoverPassword() {
        navigateTo.set(ForgotPasswordFragmentDirections.actionToMessage(
            titleResId = R.string.forgot_password_complete_title,
            messageResId = R.string.forgot_password_complete_message,
            buttonTextResId = R.string.forgot_password_complete_button
        ))
    }

    fun cancel() {
        navigateUp.call()
    }
}