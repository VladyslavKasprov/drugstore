package com.drugstore.login.login.register

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.android.core.config.ContentView
import com.android.core.config.fragment.LightNavigationBar
import com.android.core.config.fragment.NavigationBarColor
import com.android.core.fragment.BaseFragment
import com.android.core.fragment.localViewModels
import com.android.core.fragment.navigateTo
import com.android.core.fragment.observeEvent
import com.drugstore.BR
import com.drugstore.R
import com.drugstore.databinding.FragmentRegisterBinding

@ContentView(R.layout.fragment_register)
@LightNavigationBar(false)
@NavigationBarColor(R.color.translucent)
class RegisterFragment : BaseFragment<FragmentRegisterBinding>() {

    internal val model: RegisterVM by localViewModels()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        bind(BR.model, model)

        observeEvent(model.onNavigateTo) { navigateTo(it) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val extras = data?.extras
        if (resultCode == Activity.RESULT_OK && extras != null) {
            model.onActivityResult(requestCode, extras)
        }
    }
}