package com.drugstore.login.login.register

import android.content.Context
import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.core.lifecycle.BaseViewModel
import com.android.core.lifecycle.set
import com.drugstore.R
import com.drugstore.core.dialog.PICK_DATE_PICKED_KEY
import com.drugstore.core.dialog.PICK_ITEM_INT_RESULT_KEY
import com.drugstore.core.genderName
import com.drugstore.entity.Gender
import org.threeten.bp.LocalDate
import javax.inject.Inject

class RegisterVM @Inject internal constructor() : BaseViewModel() {
    private val _birthDate = MutableLiveData<LocalDate?>()
    private val _gender = MutableLiveData<Gender?>(Gender.MALE)
    val birthDate: LiveData<LocalDate?> = _birthDate
    val gender: LiveData<Gender?> = _gender

    fun pickBirthDate() {
        val birthDate = _birthDate.value ?: LocalDate.now()
        navigateTo.set(RegisterFragmentDirections.actionToPickDate(R.id.req_code_register_pick_birth_date, birthDate))
    }

    fun pickGender(context: Context) {
        val checkedItemId = (_gender.value ?: Gender.MALE).id
        val labels = Array(Gender.values.size) { i -> genderName(context, Gender.values[i]) ?: "" }
        val reqCode = R.id.req_code_register_pick_gender
        navigateTo.set(RegisterFragmentDirections.actionToPickItem(Gender.ids, checkedItemId, labels, reqCode))
    }

    internal fun onActivityResult(requestCode: Int, extras: Bundle) {
        when (requestCode) {
            R.id.req_code_register_pick_birth_date -> (extras.getSerializable(PICK_DATE_PICKED_KEY) as? LocalDate)?.let {
                pickBirthDate(it)
            }
            R.id.req_code_register_pick_gender ->
                pickGender(Gender.getById(extras.getInt(PICK_ITEM_INT_RESULT_KEY)) ?: Gender.MALE)
        }
    }

    private fun pickBirthDate(date: LocalDate) {
        _birthDate.value = date
    }

    private fun pickGender(gender: Gender) {
        _gender.value = gender
    }
}