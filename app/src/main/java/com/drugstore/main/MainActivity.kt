package com.drugstore.main

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.observe
import androidx.navigation.NavController
import com.android.core.activity.BaseActivity
import com.android.core.activity.viewModels
import com.android.core.config.ContentView
import com.android.core.config.activity.DrawerLayout
import com.android.core.config.activity.NavigationView
import com.android.core.config.activity.Theme
import com.android.core.config.activity.Toolbar
import com.drugstore.R
import com.drugstore.core.navigation.setupWithNavController
import com.drugstore.databinding.ActivityMainBinding
import com.drugstore.login.LoginActivity

@Theme(R.style.Theme)
@ContentView(R.layout.activity_main)
@DrawerLayout(R.id.dl_main)
@NavigationView(R.id.nv_main_drawer)
@Toolbar(R.id.tb_main)
class MainActivity : BaseActivity<ActivityMainBinding>() {

    internal val model: MainActivityVM by viewModels()

    private var currentNavController: LiveData<NavController>? = null
    override val navController get() = currentNavController?.value

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            setupBottomNavigationView()
            setupNavigationView()
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        setupBottomNavigationView()
        setupNavigationView()
    }

    private fun setupBottomNavigationView() {
        currentNavController = view?.bnvMain?.setupWithNavController(
            navGraphIds = listOf(R.navigation.categories, R.navigation.order_history, R.navigation.profile),
            fragmentManager = supportFragmentManager,
            containerId = R.id.fl_main_container,
            intent = intent
        )?.apply {
            observe(this@MainActivity) { setupNavigation(it) }
        }
    }

    private fun setupNavigationView() {
        view?.dlMain?.setScrimColor(Color.TRANSPARENT)
        view?.nvMainDrawer?.menu?.findItem(R.id.logout)?.setOnMenuItemClickListener {
            navigateToLoginActivity()
            true
        }
    }

    internal fun navigateToLoginActivity() {
        val intent = Intent(this, LoginActivity::class.java)
        intent.putExtra("deepLinkExtras", intent.extras)
        startActivity(intent)
        finish()
    }
}