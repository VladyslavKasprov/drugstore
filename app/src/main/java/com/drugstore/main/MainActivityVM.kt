package com.drugstore.main

import com.android.core.lifecycle.BaseViewModel
import javax.inject.Inject

internal class MainActivityVM @Inject internal constructor() : BaseViewModel()