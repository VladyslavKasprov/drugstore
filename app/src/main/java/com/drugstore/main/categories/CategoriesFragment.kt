package com.drugstore.main.categories

import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import com.android.core.config.ContentView
import com.android.core.fragment.*
import com.android.core.recyclerview.adapter.BaseAdapter
import com.android.core.recyclerview.itemdecoration.DividerItemDecoration
import com.drugstore.BR
import com.drugstore.R
import com.drugstore.databinding.FragmentCategoriesBinding
import com.drugstore.entity.Category

@ContentView(R.layout.fragment_categories)
class CategoriesFragment : BaseFragment<FragmentCategoriesBinding>() {

    internal val model: CategoriesVM by localViewModels()
    internal lateinit var adapter: BaseAdapter<Category>

    override fun onViewCreated(_view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(_view, savedInstanceState)
        setupRecyclerView()

        observeNonNull(model.categories) { adapter.submitList(it) }
        observeEvent(model.onNavigateTo) { navigateTo(it) }
    }

    private fun setupRecyclerView() {
        adapter = BaseAdapter<Category>(BR.item) { R.layout.item_category }
            .setVariable(BR.model, model)
            .setLifecycleOwner(viewLifecycleOwner)
        view?.rvCategoriesCategories?.adapter = adapter

        view?.rvCategoriesCategories?.addItemDecoration(
            DividerItemDecoration(requireContext(), R.drawable.div)
        )
        view?.rvCategoriesCategories?.addItemDecoration(
            DividerItemDecoration(requireContext(), R.drawable.div, LinearLayout.VERTICAL)
        )
    }
}