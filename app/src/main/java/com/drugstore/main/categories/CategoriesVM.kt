package com.drugstore.main.categories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.core.lifecycle.BaseViewModel
import com.android.core.lifecycle.set
import com.drugstore.entity.Category
import javax.inject.Inject

class CategoriesVM @Inject internal constructor() : BaseViewModel() {

    val categories: LiveData<List<Category>?> = MutableLiveData(
        mutableListOf(
            Category("Men's health", "Feel more confident and secure by develop a strong, good looking and healthy body", "https://image.ibb.co/kBAZ1d/2017-nissan-gt-r-inline-photo-668765-s-original.jpg"),
            Category("Women's health", "Become the better version of yourself by taking good care of your heals and beauty", "https://image.ibb.co/gBCQ7J/252978.jpg"),
            Category("STDs", "Protect your intimate life and restore your health with the best medicines", "https://di-uploads-pod2.dealerinspire.com/carlblackchevynashville/uploads/2018/05/2019-ZR1-1024x576.jpg"),
            Category("Internal medicine", "Do not let any unpleasant symptoms stand on your way to delights", "https://st.motortrend.com/uploads/sites/5/2013/04/McLaren-P1-winter-testing-motion-22.jpg"),
            Category("Travel medicine", "Take adventures and live your life for the fullest without a second thought", "https://st.motortrend.com/uploads/sites/5/2017/09/2018-Chevrolet-Camaro-ZL1-1LE-front-three-quarter-in-motion.jpg"),
            Category("General medicine", "Start living the new and better life without inconveniences and restrictions", "https://st.motortrend.com/uploads/sites/5/2019/06/2020-Toyota-Supra-Launch-Edition-front-motion-view-2.jpg")
        )
    )

    fun products(category: Category) {
        navigateTo.set(CategoriesFragmentDirections.actionCategoriesToProducts(category.name, category))
    }
}