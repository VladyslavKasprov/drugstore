package com.drugstore.main.categories.products

import android.os.Bundle
import android.view.View
import com.android.core.config.ContentView
import com.android.core.fragment.*
import com.android.core.recyclerview.adapter.BaseAdapter
import com.android.core.recyclerview.itemdecoration.DividerItemDecoration
import com.drugstore.BR
import com.drugstore.R
import com.drugstore.databinding.FragmentProductsBinding
import com.drugstore.entity.Product

@ContentView(R.layout.fragment_products)
class ProductsFragment : BaseFragment<FragmentProductsBinding>() {

    internal val model: ProductsVM by localViewModels()
    internal lateinit var adapter: BaseAdapter<Product>

    override fun onViewCreated(_view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(_view, savedInstanceState)
        setupRecyclerView()

        observeNonNull(model.products) { adapter.submitList(it) }
        observeEvent(model.onNavigateTo) { navigateTo(it) }
    }

    private fun setupRecyclerView() {
        adapter = BaseAdapter<Product>(BR.item) { R.layout.item_product }
            .setVariable(BR.model, model)
            .setLifecycleOwner(viewLifecycleOwner)
        view?.rvProductsProducts?.adapter = adapter

        view?.rvProductsProducts?.addItemDecoration(
            DividerItemDecoration(requireContext(), R.drawable.div, paddingResId = R.dimen.spacing_16)
        )
    }
}