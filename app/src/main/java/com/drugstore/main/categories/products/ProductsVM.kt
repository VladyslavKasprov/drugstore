package com.drugstore.main.categories.products

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.core.lifecycle.BaseViewModel
import com.android.core.lifecycle.set
import com.drugstore.entity.Product
import javax.inject.Inject

class ProductsVM @Inject internal constructor() : BaseViewModel() {
    val products: LiveData<List<Product>?> = MutableLiveData(
        mutableListOf(
            Product("Priligy", 12.15, listOf("https://www.doktorabc.com/uploads/catalog/122/priligy-30mg.jpg")),
            Product("Dalacin Cream", 3.35, listOf("https://www.doktorabc.com/uploads/catalog/182/dalacincream.jpg", "https://www.doktorabc.com/uploads/catalog/115/Doxycycline.jpg")),
            Product("Potenzmittel-Testpackung", 35.904848, listOf("https://www.doktorabc.com/uploads/catalog/250/ED-Trial-pack-viagra-spedra-cialis.jpg", "https://www.doktorabc.com/uploads/catalog/122/priligy-30mg.jpg", "https://www.doktorabc.com/uploads/catalog/115/Doxycycline.jpg", "https://www.doktorabc.com/uploads/catalog/250/ED-Trial-pack-viagra-spedra-cialis.jpg")),
            Product("Doxycyclin", 12.34, listOf("https://www.doktorabc.com/uploads/catalog/115/Doxycycline.jpg", "https://www.doktorabc.com/uploads/catalog/122/priligy-30mg.jpg", "https://www.doktorabc.com/uploads/catalog/182/dalacincream.jpg"))
        )
    )

    fun product(product: Product) {
        navigateTo.set(ProductsFragmentDirections.actionProductsToProduct(product.name, product))
    }
}