package com.drugstore.main.categories.products.product

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import com.android.core.config.ContentView
import com.android.core.fragment.*
import com.android.core.recyclerview.adapter.BaseAdapter
import com.drugstore.BR
import com.drugstore.R
import com.drugstore.databinding.FragmentProductBinding
import com.google.android.material.tabs.TabLayoutMediator

@ContentView(R.layout.fragment_product)
class ProductFragment : BaseFragment<FragmentProductBinding>() {

    private val args: ProductFragmentArgs by navArgs()
    internal val model: ProductVM by localViewModels()
    internal lateinit var adapter: BaseAdapter<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model.setProduct(args.product)
    }

    override fun onViewCreated(_view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(_view, savedInstanceState)
        bind(BR.model, model)
        setupRecyclerView()

        observeNonNull(model.product) { adapter.submitList(it.imageUris) }
        observeEvent(model.onNavigateTo) { navigateTo(it) }
    }

    private fun setupRecyclerView() {
        adapter = BaseAdapter(BR.item) { R.layout.item_product_image }
        view?.vpProductImages?.adapter = adapter

        TabLayoutMediator(view?.tlProductImages!!, view?.vpProductImages!!) { _, _ -> }.attach()
    }
}