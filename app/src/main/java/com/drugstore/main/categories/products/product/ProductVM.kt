package com.drugstore.main.categories.products.product

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.core.lifecycle.BaseViewModel
import com.drugstore.entity.Product
import javax.inject.Inject

class ProductVM @Inject internal constructor() : BaseViewModel() {
    private val _product: MutableLiveData<Product?> = MutableLiveData()
    val product: LiveData<Product?> = _product

    fun setProduct(product: Product) {
        _product.value = product
    }

    fun pickDosage() {

    }

    fun pickQuantity() {

    }
}