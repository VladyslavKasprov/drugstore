package com.drugstore.main.contactus

import android.os.Bundle
import android.view.View
import com.android.core.config.ContentView
import com.android.core.fragment.BaseFragment
import com.android.core.fragment.localViewModels
import com.android.core.fragment.observeNonNull
import com.android.core.recyclerview.adapter.BaseAdapter
import com.drugstore.BR
import com.drugstore.R
import com.drugstore.databinding.FragmentContactUsBinding
import com.drugstore.entity.companyinfo.*

@ContentView(R.layout.fragment_contact_us)
class ContactUsFragment : BaseFragment<FragmentContactUsBinding>() {

    internal val model: ContactUsVM by localViewModels()
    internal lateinit var adapter: BaseAdapter<CompanyInfo>

    override fun onViewCreated(_view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(_view, savedInstanceState)
        setupRecyclerView()

        observeNonNull(model.companyInfo) { adapter.submitList(it) }
    }

    private fun setupRecyclerView() {
        fun getLayoutForItemType(item: CompanyInfo) = when(item) {
            is CompanyAddress -> R.layout.item_contact_us_company_address
            is CompanyEmail -> R.layout.item_contact_us_company_email
            is CompanyPhone -> R.layout.item_contact_us_company_phone
            is CompanySchedule -> R.layout.item_contact_us_company_schedule
        }

        adapter = BaseAdapter<CompanyInfo>(BR.item) { getLayoutForItemType(it) }
            .setLifecycleOwner(viewLifecycleOwner)
        view?.rvContactUsCompanyInfo?.adapter = adapter
    }
}