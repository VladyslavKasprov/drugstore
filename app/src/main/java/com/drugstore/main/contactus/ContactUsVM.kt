package com.drugstore.main.contactus

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.core.lifecycle.BaseViewModel
import com.drugstore.entity.companyinfo.*
import javax.inject.Inject

class ContactUsVM @Inject internal constructor() : BaseViewModel() {

    val companyInfo: LiveData<List<CompanyInfo>?> = MutableLiveData(
        listOf(
            CompanyEmail("support@doctorabc.com"),
            CompanyPhone("+49 (302) 555 50 74"),
            CompanySchedule("Mon - Fri from 09:00 to 18:00"),
            CompanyAddress("Sky Marketing Ltd. (Doctorabc)\n298 Romford Road,\nE7 9HD Londod, United Kingdom"),
            CompanyAddress("335 London Road,\nReading, Berkshire,\nRG1 3NZ United Kingdom")
        )
    )
}