package com.drugstore.main.orderhistory

import android.os.Bundle
import android.view.View
import com.android.core.config.ContentView
import com.android.core.fragment.BaseFragment
import com.android.core.fragment.localViewModels
import com.android.core.fragment.observeNonNull
import com.android.core.recyclerview.adapter.BaseAdapter
import com.android.core.recyclerview.itemdecoration.DividerItemDecoration
import com.drugstore.BR
import com.drugstore.R
import com.drugstore.databinding.FragmentOrderHistoryBinding
import com.drugstore.entity.Product

@ContentView(R.layout.fragment_order_history)
class OrderHistoryFragment : BaseFragment<FragmentOrderHistoryBinding>() {

    internal val model: OrderHistoryVM by localViewModels()
    internal lateinit var adapter: BaseAdapter<Product>

    override fun onViewCreated(_view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(_view, savedInstanceState)
        setupRecyclerView()

        observeNonNull(model.products) { adapter.submitList(it) }
    }

    private fun setupRecyclerView() {
        adapter = BaseAdapter(BR.item) { R.layout.item_order_history }
        view?.rvOrderHistory?.adapter = adapter

        view?.rvOrderHistory?.addItemDecoration(
            DividerItemDecoration(requireContext(), R.drawable.div, paddingResId = R.dimen.spacing_16)
        )
    }
}