package com.drugstore.main.privacypolicy

import com.android.core.config.ContentView
import com.android.core.fragment.BaseFragment
import com.drugstore.R
import com.drugstore.databinding.FragmentPrivacyPolicyBinding

@ContentView(R.layout.fragment_privacy_policy)
class PrivacyPolicyFragment : BaseFragment<FragmentPrivacyPolicyBinding>()