package com.drugstore.main.profile

import android.os.Bundle
import com.android.core.config.ContentView
import com.android.core.config.OptionsMenu
import com.android.core.config.fragment.TopAppBarElevation
import com.android.core.fragment.BaseFragment
import com.android.core.fragment.localViewModels
import com.android.core.fragment.navigateTo
import com.android.core.fragment.observeEvent
import com.drugstore.BR
import com.drugstore.R
import com.drugstore.databinding.FragmentProfileBinding

@ContentView(R.layout.fragment_profile)
@OptionsMenu(R.menu.profile)
@TopAppBarElevation(0f)
class ProfileFragment : BaseFragment<FragmentProfileBinding>() {

    internal val model: ProfileVM by localViewModels()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        bind(BR.model, model)

        observeEvent(model.onNavigateTo) { navigateTo(it) }
    }
}