package com.drugstore.main.profile

import com.android.core.lifecycle.BaseViewModel
import com.android.core.lifecycle.set
import javax.inject.Inject

class ProfileVM @Inject internal constructor() : BaseViewModel() {

    fun addresses() {
        navigateTo.set(ProfileFragmentDirections.actionProfileToAddresses())
    }

    fun changePassword() {
        navigateTo.set(ProfileFragmentDirections.actionProfileToChangePassword())
    }
}