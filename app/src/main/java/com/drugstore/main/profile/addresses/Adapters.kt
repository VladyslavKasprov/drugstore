package com.drugstore.main.profile.addresses

import com.drugstore.entity.Address

fun addressLine1(address: Address?): String? {
    return address?.run {
//        TODO add person name
        "$firstName $lastName"
    }
}

fun addressLine2(address: Address?): String? {
    return address?.run {
        "${this.address}, $postcode"
    }
}

fun addressLine3(address: Address?): String? {
    return address?.run {
        "$city $country"
    }
}