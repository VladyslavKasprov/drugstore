package com.drugstore.main.profile.addresses

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import com.android.core.config.ContentView
import com.android.core.fragment.*
import com.android.core.getDrawable
import com.android.core.recyclerview.adapter.BaseAdapter
import com.android.core.recyclerview.itemdecoration.SwipeItemTouchCallback
import com.drugstore.BR
import com.drugstore.R
import com.drugstore.databinding.FragmentAddressesBinding
import com.drugstore.entity.Address

@ContentView(R.layout.fragment_addresses)
class AddressesFragment : BaseFragment<FragmentAddressesBinding>() {

    internal val model: AddressesVM by localViewModels()
    internal lateinit var adapter: BaseAdapter<Address>

    override fun onViewCreated(_view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(_view, savedInstanceState)
        bind(BR.model, model)
        setupRecyclerView()

        observeNonNull(model.addresses) { adapter.submitList(it) }
        observeEvent(model.onNavigateTo) { navigateTo(it) }
    }

    private fun setupRecyclerView() {
        adapter = BaseAdapter<Address>(BR.item) { R.layout.item_address }
            .setVariable(BR.model, model)
            .setLifecycleOwner(viewLifecycleOwner)
        view?.rvAddressesAddresses?.adapter = adapter

        val background = ColorDrawable(ContextCompat.getColor(requireContext(), R.color.bg_addresses_on_swipe_delete))
        val icon = requireContext().getDrawable(R.drawable.ic_delete, R.color.ic_addresses_on_swipe_delete)!!
        val swipeItemTouchCallback = SwipeItemTouchCallback(adapter, background, icon) { model.delete(it) }

        ItemTouchHelper(swipeItemTouchCallback).attachToRecyclerView(view?.rvAddressesAddresses)
    }
}