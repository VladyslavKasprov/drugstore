package com.drugstore.main.profile.addresses

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.core.lifecycle.BaseViewModel
import com.android.core.lifecycle.set
import com.drugstore.entity.Address
import javax.inject.Inject

class AddressesVM @Inject internal constructor() : BaseViewModel() {

    private val _addresses: MutableLiveData<List<Address>?> = MutableLiveData(
        mutableListOf(
            Address("Home", "Johan", "Rimpelstilzchen1", "Xaver-Fuhr-Strabe 34", null, 74321, "Tamm.", "Germany", "+48030383838"),
            Address("Home", "Johan", "Rimpelstilzchen2", "Xaver-Fuhr-Strabe 34", null, 74321, "Tamm.", "Germany", "+48030383838"),
            Address("Home", "Johan", "Rimpelstilzchen3", "Xaver-Fuhr-Strabe 34", null, 74321, "Tamm.", "Germany", "+48030383838"),
            Address("Home", "Johan", "Rimpelstilzchen4", "Xaver-Fuhr-Strabe 34", null, 74321, "Tamm.", "Germany", "+48030383838"),
            Address("Home", "Johan", "Rimpelstilzchen5", "Xaver-Fuhr-Strabe 34", null, 74321, "Tamm.", "Germany", "+48030383838"),
            Address("Home", "Johan", "Rimpelstilzchen6", "Xaver-Fuhr-Strabe 34", null, 74321, "Tamm.", "Germany", "+48030383838"),
            Address("Home", "Johan", "Rimpelstilzchen7", "Xaver-Fuhr-Strabe 34", null, 74321, "Tamm.", "Germany", "+48030383838"),
            Address("Home", "Johan", "Rimpelstilzchen8", "Xaver-Fuhr-Strabe 34", null, 74321, "Tamm.", "Germany", "+48030383838"),
            Address("Home", "Johan", "Rimpelstilzchen9", "Xaver-Fuhr-Strabe 34", null, 74321, "Tamm.", "Germany", "+48030383838"),
            Address("Home", "Johan", "Rimpelstilzchen10", "Xaver-Fuhr-Strabe 34", null, 74321, "Tamm.", "Germany", "+48030383838"),
            Address("Home", "Johan", "Rimpelstilzchen11", "Xaver-Fuhr-Strabe 34", null, 74321, "Tamm.", "Germany", "+48030383838"),
            Address("Work", "Johan", "Rimpelstilzchen", "Theodor-Adorno-Platz 6", null, 60323, "Franfurkt am Main", "Germany", "+4803033855")
        )
    )
    val addresses: LiveData<List<Address>?> = _addresses

    fun addAddress() {
        navigateTo.set(AddressesFragmentDirections.actionAddressesToPersistAddress())
    }

    fun edit(address: Address) {
//        TODO pass address.id as parameter
        navigateTo.set(AddressesFragmentDirections.actionAddressesToPersistAddress(address.name))
    }

    fun delete(address: Address) {
        val list = _addresses.value?.toMutableList()
        list?.remove(address)
        _addresses.value = list
//        _addresses.value = _addresses.value
    }
}