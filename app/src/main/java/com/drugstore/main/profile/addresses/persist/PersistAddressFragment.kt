package com.drugstore.main.profile.addresses.persist

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.navigation.fragment.navArgs
import com.android.core.config.ContentView
import com.android.core.fragment.*
import com.drugstore.BR
import com.drugstore.R
import com.drugstore.databinding.FragmentPersistAddressBinding

@ContentView(R.layout.fragment_persist_address)
class PersistAddressFragment : BaseFragment<FragmentPersistAddressBinding>() {

    private val args: PersistAddressFragmentArgs by navArgs()
    internal val model: PersistAddressVM by localViewModels()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        bind(BR.model, model)

        observeEvent(model.onNavigateTo) { navigateTo(it) }
        observeEvent(model.onNavigateUp) { navigateUp() }

        if (args.addressId != null) {
            view?.tvPersistAddressInfo?.visibility = View.VISIBLE
            view?.btnPersistAddressSubmit?.text = resources.getText(R.string.persist_address_edit_submit)
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        if (args.addressId != null) {
            requireActivity().menuInflater.inflate(R.menu.edit_address, menu)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.delete) {
            model.delete()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val extras = data?.extras
        if (resultCode == Activity.RESULT_OK && extras != null) {
            model.onActivityResult(requestCode, extras)
        }
    }
}