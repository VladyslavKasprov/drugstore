package com.drugstore.main.profile.addresses.persist

import android.content.Context
import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.core.lifecycle.BaseViewModel
import com.android.core.lifecycle.call
import com.android.core.lifecycle.set
import com.drugstore.R
import com.drugstore.core.dialog.PICK_ITEM_INT_RESULT_KEY
import com.drugstore.core.genderName
import com.drugstore.entity.Gender
import javax.inject.Inject

class PersistAddressVM @Inject internal constructor() : BaseViewModel() {
    private val _gender = MutableLiveData<Gender?>(Gender.MALE)
    val gender: LiveData<Gender?> = _gender

    fun pickGender(context: Context) {
        val checkedItemId = (_gender.value ?: Gender.MALE).id
        val labels = Array(Gender.values.size) { i -> genderName(context, Gender.values[i]) ?: "" }
        val reqCode = R.id.req_code_persist_address_pick_gender
        navigateTo.set(PersistAddressFragmentDirections.actionToPickItem(Gender.ids, checkedItemId, labels, reqCode))
    }

    internal fun delete() {
        navigateUp.call()
    }

    fun cancel() {
        navigateUp.call()
    }

    internal fun onActivityResult(requestCode: Int, extras: Bundle) {
        when (requestCode) {
            R.id.req_code_persist_address_pick_gender ->
                pickGender(Gender.getById(extras.getInt(PICK_ITEM_INT_RESULT_KEY)) ?: Gender.MALE)
        }
    }

    private fun pickGender(gender: Gender) {
        _gender.value = gender
    }
}