package com.drugstore.main.profile.changepassword

import android.os.Bundle
import com.android.core.config.ContentView
import com.android.core.fragment.*
import com.drugstore.BR
import com.drugstore.R
import com.drugstore.databinding.FragmentChangePasswordBinding

@ContentView(R.layout.fragment_change_password)
class ChangePasswordFragment : BaseFragment<FragmentChangePasswordBinding>() {

    internal val model: ChangePasswordVM by localViewModels()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        bind(BR.model, model)

        observeEvent(model.onNavigateTo) { navigateTo(it) }
        observeEvent(model.onNavigateUp) { navigateUp() }
    }
}