package com.drugstore.main.profile.changepassword

import com.android.core.lifecycle.BaseViewModel
import com.android.core.lifecycle.call
import com.android.core.lifecycle.set
import com.drugstore.R
import javax.inject.Inject

class ChangePasswordVM @Inject internal constructor() : BaseViewModel() {

    fun changePassword() {
        navigateTo.set(ChangePasswordFragmentDirections.actionToMessage(
            titleResId = R.string.change_password_complete_title,
            buttonTextResId = R.string.change_password_complete_button
        ))
    }

    fun cancel() {
        navigateUp.call()
    }
}