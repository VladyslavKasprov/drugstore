package com.drugstore.main.profile.editprofile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.android.core.config.ContentView
import com.android.core.fragment.*
import com.drugstore.BR
import com.drugstore.R
import com.drugstore.databinding.FragmentEditProfileBinding

@ContentView(R.layout.fragment_edit_profile)
class EditProfileFragment : BaseFragment<FragmentEditProfileBinding>() {

    internal val model: EditProfileVM by localViewModels()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        bind(BR.model, model)

        observeEvent(model.onNavigateTo) { navigateTo(it) }
        observeEvent(model.onNavigateUp) { navigateUp() }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val extras = data?.extras
        if (resultCode == Activity.RESULT_OK && extras != null) {
            model.onActivityResult(requestCode, extras)
        }
    }
}