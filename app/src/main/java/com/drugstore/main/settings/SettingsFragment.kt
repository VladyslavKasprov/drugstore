package com.drugstore.main.settings

import android.os.Bundle
import android.view.View
import com.android.core.config.ContentView
import com.android.core.fragment.BaseFragment
import com.android.core.fragment.localViewModels
import com.android.core.fragment.navigateTo
import com.android.core.fragment.observeEvent
import com.drugstore.BR
import com.drugstore.R
import com.drugstore.biometric.CryptoUtils
import com.drugstore.biometric.FingerprintDialogFragment
import com.drugstore.biometric.FingerprintUtils
import com.drugstore.databinding.FragmentSettingsBinding

@ContentView(R.layout.fragment_settings)
class SettingsFragment : BaseFragment<FragmentSettingsBinding>() {

    internal val model: SettingsVM by localViewModels()

    override fun onViewCreated(_view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(_view, savedInstanceState)
        bind(BR.model, model)

        observeEvent(model.onNavigateTo) { navigateTo(it) }

//        TODO refactor
        view?.vSettingsFingerprint?.setOnClickListener {
            val cryptoObject = CryptoUtils.getCryptoObject()
            val fragment = FingerprintDialogFragment()
            fragment.setTargetFragment(this, FingerprintUtils.FINGERPRINT_REQUEST_CODE_OK)
            fragment.setCryptoObject(cryptoObject)
            fragment.setAccountType(true)
            fragment.show(fragmentManager!!, FingerprintUtils.FINGERPRINT_DIALOG_FRAGMENT_TAG)
        }
    }
}