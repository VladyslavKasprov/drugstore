package com.drugstore.main.settings

import com.android.core.lifecycle.BaseViewModel
import com.android.core.lifecycle.set
import javax.inject.Inject

class SettingsVM @Inject internal constructor() : BaseViewModel() {

    fun notifications() {
        navigateTo.set(SettingsFragmentDirections.actionSettingsToNotifications())
    }

    fun languages() {
        navigateTo.set(SettingsFragmentDirections.actionSettingsToLanguages())
    }
}