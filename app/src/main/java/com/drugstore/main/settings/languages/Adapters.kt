package com.drugstore.main.settings.languages

import android.content.Context
import android.text.SpannableString
import android.text.Spanned.SPAN_INCLUSIVE_INCLUSIVE
import android.text.style.TextAppearanceSpan
import androidx.annotation.StyleRes
import com.drugstore.R

fun Context.languageName(text: CharSequence, isSelected: Boolean): CharSequence? {
    return SpannableString(text).apply {
        @StyleRes val textAppearanceResId = if (isSelected) {
            R.style.TextAppearance_Language_Name_Selected
        } else {
            R.style.TextAppearance_Language_Name
        }
        setSpan(TextAppearanceSpan(this@languageName, textAppearanceResId), 0, length, SPAN_INCLUSIVE_INCLUSIVE)
    }
}