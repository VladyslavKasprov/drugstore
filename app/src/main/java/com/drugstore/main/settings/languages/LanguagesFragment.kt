package com.drugstore.main.settings.languages

import android.os.Bundle
import android.view.View
import com.android.core.config.ContentView
import com.android.core.fragment.BaseFragment
import com.android.core.fragment.localViewModels
import com.android.core.fragment.observeNonNull
import com.android.core.recyclerview.adapter.BaseAdapter
import com.android.core.recyclerview.itemdecoration.DividerItemDecoration
import com.drugstore.BR
import com.drugstore.R
import com.drugstore.databinding.FragmentLanguagesBinding

@ContentView(R.layout.fragment_languages)
class LanguagesFragment : BaseFragment<FragmentLanguagesBinding>() {

    internal val model: LanguagesVM by localViewModels()
    internal lateinit var adapter: BaseAdapter<String>

    override fun onViewCreated(_view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(_view, savedInstanceState)
        setupRecyclerView()

        observeNonNull(model.languages) { adapter.submitList(it) }
    }

    private fun setupRecyclerView() {
        adapter = BaseAdapter<String>(BR.item) { R.layout.item_language }
            .setVariable(BR.model, model)
            .setLifecycleOwner(viewLifecycleOwner)
        view?.rvLanguagesLanguages?.adapter = adapter

        view?.rvLanguagesLanguages?.addItemDecoration(
            DividerItemDecoration(requireContext(), R.drawable.div, paddingStartResId = R.dimen.spacing_16)
        )
    }
}