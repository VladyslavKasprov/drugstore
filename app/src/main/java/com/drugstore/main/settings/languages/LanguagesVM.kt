package com.drugstore.main.settings.languages

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.core.lifecycle.BaseViewModel
import javax.inject.Inject

class LanguagesVM @Inject internal constructor() : BaseViewModel() {

    val languages: LiveData<List<String>?> = MutableLiveData(
        mutableListOf("English", "Dutch", "Francais", "Italiano", "Русский", "Polski", "Español", "Deutsche")
    )

    private val _selectedLanguage: MutableLiveData<String?> = MutableLiveData(languages.value?.get(0))
    val selectedLanguage: LiveData<String?> = _selectedLanguage

    fun selectLanguage(language: String) {
        _selectedLanguage.value = language
    }
}