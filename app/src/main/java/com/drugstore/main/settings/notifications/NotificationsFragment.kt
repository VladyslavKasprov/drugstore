package com.drugstore.main.settings.notifications

import com.android.core.config.ContentView
import com.android.core.fragment.BaseFragment
import com.drugstore.R
import com.drugstore.databinding.FragmentNotificationsBinding

@ContentView(R.layout.fragment_notifications)
class NotificationsFragment : BaseFragment<FragmentNotificationsBinding>()