package com.drugstore.main.termsandconditions

import com.android.core.config.ContentView
import com.android.core.fragment.BaseFragment
import com.drugstore.R
import com.drugstore.databinding.FragmentTermsAndConditionsBinding

@ContentView(R.layout.fragment_terms_and_conditions)
class TermsAndConditionsFragment : BaseFragment<FragmentTermsAndConditionsBinding>()