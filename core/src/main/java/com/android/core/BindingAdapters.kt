package com.android.core

import android.graphics.Typeface
import android.view.View
import android.widget.TextView
import androidx.core.view.ViewCompat
import androidx.core.view.updatePadding
import androidx.databinding.BindingAdapter

/**
 * Sets view height to a percent of display height (doesn't account for window insets).
 *
 * ConstraintLayout calculates view height based on parent's view height when using layout_constraintHeight_percent.
 * This may be undesirable when parent view has varying height (ScrollView, wrap_content, etc..).
 */
@BindingAdapter("android:bind_heightPercent")
fun setHeightPercent(view: View, percent: Double) {
    val displayHeight = view.context.resources.displayMetrics.heightPixels
    val heightFromPercent = (displayHeight * percent).toInt()
    val layoutParams = view.layoutParams
    if (layoutParams.height != heightFromPercent) {
        layoutParams.height = heightFromPercent
        view.layoutParams = layoutParams
    }
}

@BindingAdapter("android:bind_applySystemWindowInsets")
fun View.applySystemWindowInsets(flags: Int) {
    val flagTop = resources.getInteger(R.integer.flag_top)
    val flagBottom = resources.getInteger(R.integer.flag_bottom)

    ViewCompat.setOnApplyWindowInsetsListener(this) { v, insets ->
        val paddingTop = if ((flags and flagTop) == flagTop) {
            insets.systemWindowInsetTop
        } else {
            v.paddingTop
        }
        val paddingBottom = if ((flags and flagBottom) == flagBottom) {
            insets.systemWindowInsetBottom
        } else {
            v.paddingBottom
        }
        v.updatePadding(top = paddingTop, bottom = paddingBottom)
        return@setOnApplyWindowInsetsListener insets
    }
}

@BindingAdapter("android:bind_fontFamily")
fun TextView.setFontFamily(typeface: Typeface?) {
    if (this.typeface == null || this.typeface != typeface) {
        this.typeface = typeface
    }
}