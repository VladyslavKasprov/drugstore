package com.android.core

import android.content.res.TypedArray
import androidx.annotation.StyleableRes

internal fun TypedArray.getColorOrNull(@StyleableRes index: Int) = if (hasValue(index)) {
    getColor(index, 0)
} else {
    null
}

internal fun TypedArray.getBooleanOrNull(@StyleableRes index: Int) = if (hasValue(index)) {
    getBoolean(index, false)
} else {
    null
}