package com.android.core

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat

fun Context.getFloat(@DimenRes id: Int) = ResourcesCompat.getFloat(resources, id)

fun Collection<Any>?.isEmpty() = this?.isEmpty() ?: true

fun Context.getDrawable(@DrawableRes drawableResId: Int, @ColorRes colorResId: Int): Drawable? {
    val drawable = ContextCompat.getDrawable(this, drawableResId)
    val color = ContextCompat.getColor(this, colorResId)
    return drawable?.tint(color)
}

fun Drawable.tint(@ColorInt color: Int): Drawable =
    DrawableCompat.wrap(this).apply { DrawableCompat.setTint(this, color) }