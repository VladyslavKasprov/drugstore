package com.android.core.activity

import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.annotation.MenuRes
import androidx.annotation.StyleRes
import androidx.appcompat.app.AppCompatActivity
import com.android.core.config.ContentView
import com.android.core.config.OptionsMenu
import com.android.core.config.TopAppBarTitleTextAppearance
import com.android.core.config.activity.*

internal class ActivityConfig internal constructor(
    @param:StyleRes @field:StyleRes val themeResId: Int?,
    @param:LayoutRes @field:LayoutRes val layoutResId: Int?,
    @param:IdRes @field:IdRes val navHostFragmentResId: Int?,
    val topLevelDestinationsResIds: IntArray?,
    @param:IdRes @field:IdRes val drawerLayoutResId: Int?,
    @param:IdRes @field:IdRes val navigationViewResId: Int?,
    @param:IdRes @field:IdRes val toolbarResId: Int?,
    @param:MenuRes @field:MenuRes val optionsMenuResId: Int?,
    @param:StyleRes @field:StyleRes val topAppBarTitleTextAppearanceResId: Int?
)

internal fun AppCompatActivity.collectConfig(): ActivityConfig {
    return ActivityConfig(
        javaClass.getAnnotation(Theme::class.java)?.value,
        javaClass.getAnnotation(ContentView::class.java)?.value,
        javaClass.getAnnotation(NavHostFragment::class.java)?.value,
        javaClass.getAnnotation(TopLevelDestinations::class.java)?.value,
        javaClass.getAnnotation(DrawerLayout::class.java)?.value,
        javaClass.getAnnotation(NavigationView::class.java)?.value,
        javaClass.getAnnotation(Toolbar::class.java)?.value,
        javaClass.getAnnotation(OptionsMenu::class.java)?.value,
        javaClass.getAnnotation(TopAppBarTitleTextAppearance::class.java)?.value
    )
}