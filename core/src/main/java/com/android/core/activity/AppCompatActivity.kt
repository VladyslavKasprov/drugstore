package com.android.core.activity

import android.widget.Toast
import androidx.annotation.MainThread
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.android.core.lifecycle.Event

@MainThread fun <T> AppCompatActivity.observe(
    liveData: LiveData<T?>,
    onChanged: (T?) -> Unit
) {
    liveData.observe(this, Observer { onChanged(it) })
}

@MainThread fun <T> AppCompatActivity.observeNonNull(
    liveData: LiveData<T?>,
    onChanged: (T) -> Unit
) {
    liveData.observe(this, Observer { it?.let(onChanged) })
}

@MainThread fun <T> AppCompatActivity.observeEvent(
    liveData: LiveData<Event<T>?>,
    onChanged: (T) -> Unit
) {
    liveData.observe(this, Observer { it?.contentIfNotHandled?.let(onChanged) })
}

fun AppCompatActivity.showToast(@StringRes messageResId: Int) {
    Toast.makeText(this, messageResId, Toast.LENGTH_LONG).show()
}

fun AppCompatActivity.showDialog(dialogFragment: DialogFragment, tag: String) {
    dialogFragment.showNow(supportFragmentManager, tag)
}

fun AppCompatActivity.removeFragment(tag: String) {
    val fragment = supportFragmentManager.findFragmentByTag(tag)
    if (fragment != null) {
        supportFragmentManager.beginTransaction().remove(fragment).commit()
    }
}

fun AppCompatActivity.isFragmentAdded(tag: String): Boolean {
    val fragment = supportFragmentManager.findFragmentByTag(tag)
    return fragment != null && fragment.isAdded
}