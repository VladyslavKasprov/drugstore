package com.android.core.activity

import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.View.*
import androidx.activity.viewModels
import androidx.annotation.DrawableRes
import androidx.annotation.MainThread
import androidx.annotation.StyleRes
import androidx.appcompat.widget.Toolbar
import androidx.core.view.ViewCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI.onNavDestinationSelected
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

abstract class BaseActivity<B : ViewDataBinding> : DaggerAppCompatActivity() {

    private val config = collectConfig()

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory

    protected var view: B? = null

    protected open val navController get() = config.navHostFragmentResId?.let { findNavController(it) }
    protected val currentDestinationId get() = navController?.currentDestination?.id ?: 0
    protected val currentDestinationParentId get() = navController?.currentDestination?.parent?.id ?: 0

    private val drawerLayout get() = config.drawerLayoutResId?.let { findViewById<DrawerLayout>(it) }
    private val navigationView get() = config.navigationViewResId?.let { findViewById<NavigationView>(it) }
    private val toolbar get() = config.toolbarResId?.let { findViewById<Toolbar>(it) }
    private var appBarConfiguration: AppBarConfiguration? = null

    private val topAppBarBackground: Drawable? by lazy { toolbar?.background }
    private val topAppBarElevation: Float by lazy { toolbar?.let { ViewCompat.getElevation(it) } ?: 0f }
    val topAppBar: View? get() = toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        config.themeResId.takeIf { it != 0 }?.let { setTheme(it) }
        if (Build.VERSION.SDK_INT >= 21) {
            window.decorView.systemUiVisibility = SYSTEM_UI_FLAG_LAYOUT_STABLE or SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        }
        super.onCreate(savedInstanceState)

        config.layoutResId.takeIf { it != 0 }?.let {
            view = DataBindingUtil.setContentView(this, it)
            view?.lifecycleOwner = this
//            Read default background and elevation.
            topAppBarBackground
            topAppBarElevation
        }

        if (savedInstanceState == null) {
            setupNavigation(navController)
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        setupNavigation(navController)
    }

    protected fun setupNavigation(navController: NavController?) {
        toolbar?.let { toolbar ->
            setSupportActionBar(toolbar)

            appBarConfiguration = navController?.let {
                val appBarConfiguration = config.topLevelDestinationsResIds?.run {
                    AppBarConfiguration(toSet(), drawerLayout)
                } ?: AppBarConfiguration(it.graph, drawerLayout)
                setupActionBarWithNavController(it, appBarConfiguration)
                appBarConfiguration
            }
        }

        navController?.let { navigationView?.setupWithNavController(it) }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        return config.optionsMenuResId.takeIf { it != 0 }?.let {
            menuInflater.inflate(it, menu)
            true
        } ?: super.onCreateOptionsMenu(menu)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navigateUp = navController?.run { appBarConfiguration?.let { navigateUp(it) } } ?: false
        return navigateUp || super.onSupportNavigateUp()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val onNavDestinationSelected = navController?.let { onNavDestinationSelected(item, it) } ?: false
        return onNavDestinationSelected || super.onOptionsItemSelected(item)
    }

    public override fun onDestroy() {
        super.onDestroy()
        view?.unbind()
        view = null
    }

    fun setDefaultTopAppBarBackground() {
        toolbar?.background = topAppBarBackground
    }

    fun setTopAppBarBackground(@DrawableRes backgroundResId: Int) {
        toolbar?.setBackgroundResource(backgroundResId)
    }

    fun setDefaultTopAppBarElevation() {
        toolbar?.let { ViewCompat.setElevation(it, topAppBarElevation) }
    }

    fun setTopAppBarElevation(elevation: Float) {
        toolbar?.let { ViewCompat.setElevation(it, elevation) }
    }

    fun setDefaultTopAppBarTitleTextAppearance() {
        config.topAppBarTitleTextAppearanceResId?.let { setTopAppBarTitleTextAppearance(it) }
    }

    fun setTopAppBarTitleTextAppearance(@StyleRes textAppearanceRedId: Int) {
        toolbar?.setTitleTextAppearance(this, textAppearanceRedId)
    }

    protected fun navigateTo(directions: NavDirections) {
        navController?.navigate(directions)
    }
}

@MainThread
inline fun <reified VM : ViewModel, B : ViewDataBinding> BaseActivity<B>.viewModels(): Lazy<VM> {
    return viewModels { viewModelFactory }
}