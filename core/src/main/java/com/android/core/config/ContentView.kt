package com.android.core.config

import androidx.annotation.LayoutRes
import java.lang.annotation.Inherited

@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
@Inherited
@Retention(AnnotationRetention.RUNTIME)
annotation class ContentView(@LayoutRes val value: Int)