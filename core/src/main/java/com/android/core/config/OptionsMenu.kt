package com.android.core.config

import androidx.annotation.MenuRes
import java.lang.annotation.Inherited

@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
@Inherited
@Retention(AnnotationRetention.RUNTIME)
annotation class OptionsMenu(@MenuRes val value: Int)