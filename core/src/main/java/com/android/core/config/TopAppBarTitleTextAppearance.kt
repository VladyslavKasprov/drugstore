package com.android.core.config

import androidx.annotation.StyleRes
import java.lang.annotation.Inherited

@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
@Inherited
@Retention(AnnotationRetention.RUNTIME)
annotation class TopAppBarTitleTextAppearance(@StyleRes val value: Int)