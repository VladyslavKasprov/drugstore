package com.android.core.config.activity

import androidx.annotation.StyleRes
import java.lang.annotation.Inherited

@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
@Inherited
@Retention(AnnotationRetention.RUNTIME)
annotation class Theme(@StyleRes val value: Int)