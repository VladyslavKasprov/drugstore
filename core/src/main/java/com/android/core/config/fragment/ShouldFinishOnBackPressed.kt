package com.android.core.config.fragment

import java.lang.annotation.Inherited

@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
@Inherited
@Retention(AnnotationRetention.RUNTIME)
annotation class ShouldFinishOnBackPressed(val value: Boolean)