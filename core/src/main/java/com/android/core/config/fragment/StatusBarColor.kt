package com.android.core.config.fragment

import androidx.annotation.ColorRes
import java.lang.annotation.Inherited

@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
@Inherited
@Retention(AnnotationRetention.RUNTIME)
annotation class StatusBarColor(@ColorRes val value: Int)