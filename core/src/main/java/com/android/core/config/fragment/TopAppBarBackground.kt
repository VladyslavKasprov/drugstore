package com.android.core.config.fragment

import androidx.annotation.DrawableRes
import java.lang.annotation.Inherited

@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
@Inherited
@Retention(AnnotationRetention.RUNTIME)
annotation class TopAppBarBackground(@DrawableRes val value: Int)