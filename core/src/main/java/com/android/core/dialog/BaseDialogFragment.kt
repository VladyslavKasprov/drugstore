package com.android.core.dialog

import androidx.annotation.IdRes
import androidx.annotation.MainThread
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.navGraphViewModels
import com.android.core.lifecycle.Event
import dagger.android.support.DaggerDialogFragment
import javax.inject.Inject

open class BaseDialogFragment : DaggerDialogFragment() {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
}

@MainThread
inline fun <reified VM : ViewModel> BaseDialogFragment.localViewModels(): Lazy<VM> {
    return viewModels { viewModelFactory }
}

@MainThread
inline fun <reified VM : ViewModel> BaseDialogFragment.scopedViewModels(@IdRes navGraphId: Int): Lazy<VM> {
    return navGraphViewModels(navGraphId) { viewModelFactory }
}

@MainThread
inline fun <reified VM : ViewModel> BaseDialogFragment.globalViewModels(): Lazy<VM> {
    return activityViewModels { viewModelFactory }
}

@MainThread fun <T> BaseDialogFragment.observe(
    liveData: LiveData<T?>,
    onChanged: (T?) -> Unit
) {
    liveData.observe(this, Observer { onChanged(it) })
}

@MainThread fun <T> BaseDialogFragment.observeNonNull(
    liveData: LiveData<T?>,
    onChanged: (T) -> Unit
) {
    liveData.observe(this, Observer { it?.let(onChanged) })
}

@MainThread fun <T> BaseDialogFragment.observeEvent(
    liveData: LiveData<Event<T>?>,
    onChanged: (T) -> Unit
) {
    liveData.observe(this, Observer { it?.contentIfNotHandled?.let(onChanged) })
}