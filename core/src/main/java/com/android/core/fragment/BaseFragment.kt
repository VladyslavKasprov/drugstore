package com.android.core.fragment

import android.animation.Animator
import android.animation.AnimatorInflater
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.activity.OnBackPressedCallback
import androidx.annotation.IdRes
import androidx.annotation.MainThread
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.view.updatePadding
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.navGraphViewModels
import com.android.core.R
import com.android.core.activity.BaseActivity
import com.android.core.lifecycle.Event
import dagger.android.support.DaggerFragment
import javax.inject.Inject

abstract class BaseFragment<B : ViewDataBinding> : DaggerFragment() {

    private val config by lazy { collectConfig() }

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory

    protected var view: B? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)

        config.layoutResId.takeIf { it != 0 }?.let {
            view = DataBindingUtil.inflate(inflater, it, container, false)
            view?.lifecycleOwner = viewLifecycleOwner

            if (config.shouldInsetTopAppBar == null || config.shouldInsetTopAppBar == true) {
                (requireActivity() as? BaseActivity<*>)?.topAppBar?.run { insetViewTop(this) }
            }

            val window = requireActivity().window

            if (Build.VERSION.SDK_INT >= 21) {
                config.statusBarColor?.run { window.statusBarColor = this }
                config.navigationBarColor?.run { window.navigationBarColor = this }
            }

            if (Build.VERSION.SDK_INT >= 23) {
                window.decorView.systemUiVisibility = when (config.lightStatusBar) {
                    true -> window.decorView.systemUiVisibility or SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                    false -> window.decorView.systemUiVisibility and SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
                    else -> window.decorView.systemUiVisibility
                }
            } else if (Build.VERSION.SDK_INT >= 21 && config.lightStatusBar == true) {
                window.statusBarColor = ContextCompat.getColor(requireContext(), R.color.translucent)
            }

            if (Build.VERSION.SDK_INT >= 26) {
                window.decorView.systemUiVisibility = when (config.lightNavigationBar) {
                    true -> window.decorView.systemUiVisibility or SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
                    false -> window.decorView.systemUiVisibility and SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR.inv()
                    else -> window.decorView.systemUiVisibility
                }
            } else if (Build.VERSION.SDK_INT >= 21 && config.lightNavigationBar == true) {
                window.navigationBarColor = ContextCompat.getColor(requireContext(), R.color.translucent)
            }

            return view?.root
        }
        return null
    }

    private fun insetViewTop(v: View) {
        if (v.height > 0) {
            view?.root?.updatePadding(top = v.height)
        } else {
            v.addOnLayoutChangeListener(object : OnLayoutChangeListener {
                override fun onLayoutChange(v: View, l: Int, t: Int, r: Int, b: Int, oL: Int, oT: Int, oR: Int, oB: Int) {
                    if (v.height > 0) {
                        v.removeOnLayoutChangeListener(this)
                        view?.root?.updatePadding(top = v.height)
                    }
                }
            })
        }
    }

    override fun onViewCreated(_view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(_view, savedInstanceState)
        val baseActivity = requireActivity() as BaseActivity<*>

        config.topAppBarBackgroundResId?.let {
            baseActivity.setTopAppBarBackground(it)
        } ?: baseActivity.setDefaultTopAppBarBackground()

        config.topAppBarElevationResId?.let {
            baseActivity.setTopAppBarElevation(it)
        } ?: baseActivity.setDefaultTopAppBarElevation()

        config.topAppBarTitleTextAppearanceResId?.takeIf { it != 0 }?.let {
            baseActivity.setTopAppBarTitleTextAppearance(it)
        } ?: baseActivity.setDefaultTopAppBarTitleTextAppearance()

        if (config.shouldFinishOnBackPressed == true) {
            requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    requireActivity().finish()
                }
            })
        }
    }

    protected fun bind(id: Int, variable: Any) {
        if (view?.setVariable(id, variable) == false) {
            throw IllegalArgumentException("Binding failed! Incorrect binding variable name!")
        }
        view?.executePendingBindings()
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        if (config.shouldInheritOptionsMenu == null || config.shouldInheritOptionsMenu == false) {
            menu.clear()
        }
        config.optionsMenuResId.takeIf { it != 0 }?.let {
            requireActivity().menuInflater.inflate(it, menu)
        }
    }

//    TODO Remove this workaround upon release of `fragment 1.2.0-alpha02` which is expected to fix the issue https://issuetracker.google.com/issues/37036000
    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        if (enter || nextAnim == 0 || "animator" == resources.getResourceTypeName(nextAnim)) {
            return super.onCreateAnimation(transit, enter, nextAnim)
        }

        val nextAnimation = AnimationUtils.loadAnimation(context, nextAnim)
        nextAnimation.setAnimationListener(object : Animation.AnimationListener {

            private var startZ = 0f

            override fun onAnimationStart(animation: Animation) {
                getView()?.apply {
                    startZ = ViewCompat.getTranslationZ(this)
                    ViewCompat.setTranslationZ(this, -1f)
                }
            }

            override fun onAnimationEnd(animation: Animation) {
                getView()?.apply { post { ViewCompat.setTranslationZ(this, startZ) } }
            }

            override fun onAnimationRepeat(animation: Animation) {}
        })
        return nextAnimation
    }

//    TODO Remove this workaround upon release of `fragment 1.2.0-alpha02` which is expected to fix the issue https://issuetracker.google.com/issues/37036000
    override fun onCreateAnimator(transit: Int, enter: Boolean, nextAnim: Int): Animator? {
        if (enter || nextAnim == 0 || "anim" == resources.getResourceTypeName(nextAnim)) {
            return super.onCreateAnimator(transit, enter, nextAnim)
        }

        val nextAnimator = AnimatorInflater.loadAnimator(context, nextAnim)
        nextAnimator.addListener(object : Animator.AnimatorListener {

            private var startZ = 0f

            override fun onAnimationStart(animator: Animator) {
                getView()?.apply {
                    startZ = ViewCompat.getTranslationZ(this)
                    ViewCompat.setTranslationZ(this, -1f)
                }
            }

            override fun onAnimationEnd(animator: Animator) {
                getView()?.apply { post { ViewCompat.setTranslationZ(this, startZ) } }
            }

            override fun onAnimationRepeat(animator: Animator) {}

            override fun onAnimationCancel(animation: Animator?) {}
        })
        return nextAnimator
    }

    override fun onDestroyView() {
        super.onDestroyView()
        view?.unbind()
        view = null
    }
}

@MainThread
inline fun <reified VM : ViewModel, B : ViewDataBinding> BaseFragment<B>.localViewModels(): Lazy<VM> {
    return viewModels { viewModelFactory }
}

@MainThread
inline fun <reified VM : ViewModel, B : ViewDataBinding> BaseFragment<B>.scopedViewModels(
        @IdRes navGraphId: Int
): Lazy<VM> {
    return navGraphViewModels(navGraphId) { viewModelFactory }
}

@MainThread
inline fun <reified VM : ViewModel, B : ViewDataBinding> BaseFragment<B>.globalViewModels(): Lazy<VM> {
    return activityViewModels { viewModelFactory }
}

@MainThread fun <T> BaseFragment<*>.observe(
    liveData: LiveData<T?>,
    onChanged: (T?) -> Unit
) {
    liveData.observe(viewLifecycleOwner, Observer { onChanged(it) })
}

@MainThread fun <T> BaseFragment<*>.observeNonNull(
    liveData: LiveData<T?>,
    onChanged: (T) -> Unit
) {
    liveData.observe(viewLifecycleOwner, Observer { it?.let(onChanged) })
}

@MainThread fun <T> BaseFragment<*>.observeEvent(
    liveData: LiveData<Event<T>?>,
    onChanged: (T) -> Unit
) {
    liveData.observe(viewLifecycleOwner, Observer { it?.contentIfNotHandled?.let(onChanged) })
}