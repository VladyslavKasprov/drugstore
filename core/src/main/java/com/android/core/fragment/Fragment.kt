package com.android.core.fragment

import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController

fun Fragment.navigateTo(directions: NavDirections) {
    findNavController().navigate(directions)
}

fun Fragment.navigateUp() {
    findNavController().navigateUp()
}

fun Fragment.showToast(@StringRes messageResId: Int) {
    Toast.makeText(requireContext(), messageResId, Toast.LENGTH_LONG).show()
}

fun Fragment.showDialog(dialogFragment: DialogFragment) {
    dialogFragment.showNow(childFragmentManager, "")
}

fun Fragment.showDialog(dialogFragment: DialogFragment, requestCode: Int) {
    dialogFragment.setTargetFragment(this, requestCode)
    dialogFragment.showNow(requireFragmentManager(), "")
}