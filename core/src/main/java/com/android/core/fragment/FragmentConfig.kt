package com.android.core.fragment

import androidx.annotation.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.android.core.R
import com.android.core.config.ContentView
import com.android.core.config.OptionsMenu
import com.android.core.config.TopAppBarTitleTextAppearance
import com.android.core.config.fragment.*
import com.android.core.getBooleanOrNull
import com.android.core.getColorOrNull

internal class FragmentConfig internal constructor(
    @param:LayoutRes @field:LayoutRes val layoutResId: Int?,
    val shouldInheritOptionsMenu: Boolean?,
    @param:MenuRes @field:MenuRes val optionsMenuResId: Int?,
    @param:DrawableRes @field:DrawableRes val topAppBarBackgroundResId: Int?,
    val topAppBarElevationResId: Float?,
    @param:StyleRes @field:StyleRes val topAppBarTitleTextAppearanceResId: Int?,
    val shouldInsetTopAppBar: Boolean?,
    val shouldFinishOnBackPressed: Boolean?,
    @param:ColorInt @field:ColorInt val statusBarColor: Int?,
    @param:ColorInt @field:ColorInt val navigationBarColor: Int?,
    val lightStatusBar: Boolean?,
    val lightNavigationBar: Boolean?
)

internal fun Fragment.collectConfig(): FragmentConfig {
    val typedArray = requireContext().obtainStyledAttributes(R.styleable.Core)

    val statusBarColor = javaClass.getAnnotation(StatusBarColor::class.java)?.value?.let {
        ContextCompat.getColor(requireContext(), it)
    } ?: typedArray.getColorOrNull(R.styleable.Core_core_statusBarColor)

    val navigationBarColor = javaClass.getAnnotation(NavigationBarColor::class.java)?.value?.let {
        ContextCompat.getColor(requireContext(), it)
    } ?: typedArray.getColorOrNull(R.styleable.Core_core_navigationBarColor)

    val lightStatusBar = javaClass.getAnnotation(LightStatusBar::class.java)?.value
        ?: typedArray.getBooleanOrNull(R.styleable.Core_core_windowLightStatusBar)

    val lightNavigationBar = javaClass.getAnnotation(LightNavigationBar::class.java)?.value
        ?: typedArray.getBooleanOrNull(R.styleable.Core_core_windowLightNavigationBar)

    typedArray.recycle()

    return FragmentConfig(
        javaClass.getAnnotation(ContentView::class.java)?.value,
        javaClass.getAnnotation(ShouldInheritOptionsMenu::class.java)?.value,
        javaClass.getAnnotation(OptionsMenu::class.java)?.value,
        javaClass.getAnnotation(TopAppBarBackground::class.java)?.value,
        javaClass.getAnnotation(TopAppBarElevation::class.java)?.value,
        javaClass.getAnnotation(TopAppBarTitleTextAppearance::class.java)?.value,
        javaClass.getAnnotation(ShouldInsetTopAppBar::class.java)?.value,
        javaClass.getAnnotation(ShouldFinishOnBackPressed::class.java)?.value,
        statusBarColor,
        navigationBarColor,
        lightStatusBar,
        lightNavigationBar
    )
}