package com.android.core.lifecycle;

import android.os.Handler;
import androidx.lifecycle.MediatorLiveData;

public class AutoUpdateLiveData<T> extends MediatorLiveData<T> {

    private static final long DEFAULT_UPDATE_INTERVAL_MILLIS = 1000;

    final long updateIntervalMillis;
    final Handler updateHandler = new Handler();

    public AutoUpdateLiveData() {
        this(DEFAULT_UPDATE_INTERVAL_MILLIS);
    }

    public AutoUpdateLiveData(long updateIntervalMillis) {
        this.updateIntervalMillis = updateIntervalMillis;
    }

    void update() {
        setValue(getValue());
        if (hasActiveObservers()) {
            updateHandler.postDelayed(this::update, updateIntervalMillis);
        }
    }

    @Override
    protected void onActive() {
        super.onActive();
        update();
    }
}