package com.android.core.lifecycle

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations

@MainThread
fun <X, Y> LiveData<X?>.mapNonNull(map: (X) -> Y?): LiveData<Y?> {
    val result = MediatorLiveData<Y?>()
    result.addSource(this) { it?.let { result.value = map(it) } }
    return result
}

@MainThread
fun <X, Y> LiveData<X?>.switchMapNonNull(switchMap: (X) -> LiveData<Y?>): LiveData<Y?> {
    return Transformations.switchMap(this) { it?.let(switchMap) }
}

@MainThread
fun MutableLiveData<Event<Any>?>.call() {
    value = Event(Unit)
}

@MainThread
fun <T> MutableLiveData<Event<T>?>.set(content: T) {
    value = Event(content)
}

@WorkerThread
fun MutableLiveData<Event<Any>?>.postCall() {
    postValue(Event(Unit))
}

@WorkerThread
fun <T> MutableLiveData<Event<T>?>.post(content: T) {
    postValue(Event(content))
}

@MainThread
fun <T, S> MediatorLiveData<T?>.addNonNullSource(source: LiveData<S?>, onChanged: (S) -> Unit) {
    addSource(source) { it?.let(onChanged) }
}