package com.android.core.recyclerview.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class BaseAdapter<T> extends RecyclerView.Adapter<BaseAdapter.ViewHolder> {

    private static final DiffUtil.ItemCallback DEFAULT_DIFF_CALLBACK = new DiffUtil.ItemCallback() {
        @Override
        public boolean areItemsTheSame(@NonNull Object oldItem, @NonNull Object newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull Object oldItem, @NonNull Object newItem) {
            return true;
        }
    };
    private final AsyncListDiffer<T> differ = new AsyncListDiffer<T>(this, DEFAULT_DIFF_CALLBACK);

    private final ViewTypeFactory<T> viewTypeFactory;
    @Nullable private final Integer itemVariableId;
    @Nullable private Integer variableId;
    @Nullable private Object variable;
    @Nullable private LifecycleOwner lifecycleOwner;

    public BaseAdapter(ViewTypeFactory<T> viewTypeFactory) {
        this(null, viewTypeFactory);
    }

    public BaseAdapter(@Nullable Integer itemVariableId, ViewTypeFactory<T> viewTypeFactory) {
        this.itemVariableId = itemVariableId;
        this.viewTypeFactory = viewTypeFactory;
    }

    public BaseAdapter<T> setVariable(int variableId, @Nullable Object value) {
        this.variableId = variableId;
        this.variable = value;
        return this;
    }

    public BaseAdapter<T> setLifecycleOwner(@Nullable LifecycleOwner lifecycleOwner) {
        this.lifecycleOwner = lifecycleOwner;
        return this;
    }

    @NonNull public List<T> getCurrentList() {
        return differ.getCurrentList();
    }

    public void submitList(@Nullable List<T> newList) {
        differ.submitList(newList);
    }

    public void addListListener(@NonNull AsyncListDiffer.ListListener<T> listener) {
        differ.addListListener(listener);
    }

    public void removeListListener(@NonNull AsyncListDiffer.ListListener<T> listener) {
        differ.removeListListener(listener);
    }

    @Override
    public int getItemCount() {
        return differ.getCurrentList().size();
    }

    @Override
    public int getItemViewType(int position) {
        return viewTypeFactory.getViewType(differ.getCurrentList().get(position));
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding view = DataBindingUtil.inflate(inflater, viewType, parent, false);
        if (view == null) {
            throw new IllegalArgumentException("Binding for this viewType doesn't exist");
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        if (itemVariableId != null) {
            viewHolder.bind(itemVariableId, differ.getCurrentList().get(position));
        }
        if (variableId != null) {
            viewHolder.bind(variableId, variable);
        }
        viewHolder.setLifecycleOwner(lifecycleOwner);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ViewDataBinding view;

        ViewHolder(ViewDataBinding view) {
            super(view.getRoot());
            this.view = view;
        }

        void bind(int variableId, @Nullable Object value) {
            if (!view.setVariable(variableId, value)) {
                throw new IllegalArgumentException("Wasn't able to bind variable to BaseAdapter.ViewHolder");
            }
            view.executePendingBindings();
        }

        void setLifecycleOwner(@Nullable LifecycleOwner lifecycleOwner) {
            view.setLifecycleOwner(lifecycleOwner);
            view.executePendingBindings();
        }
    }
}