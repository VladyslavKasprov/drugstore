package com.android.core.recyclerview.adapter;

import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Arrays;
import java.util.List;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

public class ComposedAdapter extends RecyclerView.Adapter<ComposedAdapter.ViewHolder> {

    private final List<RecyclerView> recyclerViews;

    private ComposedAdapter(List<RecyclerView> recyclerViews) {
        this.recyclerViews = recyclerViews;
    }

    public static ComposedAdapter of(@NonNull RecyclerView... recyclerViews) {
        return new ComposedAdapter(Arrays.asList(recyclerViews));
    }

    @Override
    public int getItemCount() {
        return recyclerViews.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LinearLayout linearLayout = new LinearLayout(parent.getContext());
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT));
        return new ViewHolder(linearLayout);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ViewGroup.LayoutParams layoutParams = new LinearLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT);
        holder.linearLayout.addView(recyclerViews.get(position), layoutParams);
    }

    @Override
    public void onViewRecycled(@NonNull ViewHolder holder) {
        holder.linearLayout.removeAllViews();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout linearLayout;

        ViewHolder(@NonNull LinearLayout linearLayout) {
            super(linearLayout);
            this.linearLayout = linearLayout;
        }
    }
}