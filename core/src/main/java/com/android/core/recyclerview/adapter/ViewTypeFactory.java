package com.android.core.recyclerview.adapter;

public interface ViewTypeFactory<T> {

    int getViewType(T item);
}