package com.android.core.recyclerview.itemdecoration

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.DimenRes
import androidx.annotation.Dimension
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlin.math.roundToInt

class DividerItemDecoration internal constructor(
    private val divider: Drawable?,
    private val orientation: Int = LinearLayout.HORIZONTAL,
    @param:Dimension @field:Dimension private val paddingEnd: Int = 0,
    @param:Dimension @field:Dimension private val paddingStart: Int = 0,
    private val isLastDrawn: Boolean = false,
    private val isCentered: Boolean = false
) : RecyclerView.ItemDecoration() {
    private val bounds = Rect()

    private val dividerIntrinsicWidth: Int get() = divider?.intrinsicWidth ?: 0
    private val dividerIntrinsicHeight: Int get() = divider?.intrinsicHeight ?: 0

    private val offsetForHorizontalDivider: Int get() = if (isCentered) dividerIntrinsicHeight / 2 else 0
    private val offsetForVerticalDivider: Int get() = if (isCentered) dividerIntrinsicWidth / 2 else 0

    override fun onDraw(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        if (parent.layoutManager == null || divider == null) {
            return
        }
        if (orientation == LinearLayout.HORIZONTAL) {
            drawHorizontal(canvas, parent)
        } else {
            drawVertical(canvas, parent)
        }
    }

    private fun drawHorizontal(canvas: Canvas, parent: RecyclerView) {
        canvas.save()

        var left: Int
        var right: Int
        if (parent.clipToPadding) {
            left = parent.paddingLeft
            right = parent.width - parent.paddingRight
            canvas.clipRect(left, parent.paddingTop, right, parent.height - parent.paddingBottom)
        } else {
            left = 0
            right = parent.width
        }
        right -= paddingEnd
        left += paddingStart

        val childCount = parent.childCount
        val spanCount = getSpanCount(parent)

        for (i in 0 until childCount) {
            if (isHorizontalDividerDrawnFor(i, spanCount, childCount)) {
                val child = parent.getChildAt(i)
                parent.getDecoratedBoundsWithMargins(child, bounds)
                val bottom = bounds.bottom + child.translationY.roundToInt() + offsetForHorizontalDivider
                val top = bottom - dividerIntrinsicHeight
                divider?.setBounds(left, top, right, bottom)
                divider?.draw(canvas)
            }
        }

        canvas.restore()
    }

    private fun isVerticalDividerDrawnFor(position: Int, spanCount: Int): Boolean {
        return isLastDrawn || !isLastInRow(position, spanCount)
    }

    private fun drawVertical(canvas: Canvas, parent: RecyclerView) {
        canvas.save()

        val childCount = parent.childCount
        val spanCount = getSpanCount(parent)

        for (i in 0 until childCount) {
            if (isVerticalDividerDrawnFor(i, spanCount)) {
                val child = parent.getChildAt(i)
                parent.layoutManager?.getDecoratedBoundsWithMargins(child, bounds)
                val bottom = bounds.bottom - paddingEnd
                val right = bounds.right + child.translationX.roundToInt() + offsetForVerticalDivider
                val left = right - dividerIntrinsicWidth
                val top = bounds.top + paddingStart
                divider?.setBounds(left, top, right, bottom)
                divider?.draw(canvas)
            }
        }

        canvas.restore()
    }

    private fun isHorizontalDividerDrawnFor(position: Int, spanCount: Int, totalCount: Int): Boolean {
        return isLastDrawn || !isInTheLastRow(position, spanCount, totalCount)
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        if (divider == null) {
            outRect.set(0, 0, 0, 0)
        } else {
            if (orientation == LinearLayout.HORIZONTAL) {
                outRect.set(0, 0, 0, divider.intrinsicHeight)
            } else {
                outRect.set(0, 0, divider.intrinsicWidth, 0)
            }
        }
    }

    companion object {

        private fun isLastInRow(position: Int, spanCount: Int): Boolean {
            return (position + 1) % spanCount == 0
        }

        private fun isInTheLastRow(position: Int, spanCount: Int, totalCount: Int): Boolean {
            return position >= totalCount - spanCount
        }

        private fun getSpanCount(parent: RecyclerView) = (parent.layoutManager as? GridLayoutManager)?.spanCount ?: 1
    }
}

fun DividerItemDecoration(
    context: Context,
    @DrawableRes dividerResId: Int,
    orientation: Int = LinearLayout.HORIZONTAL,
    @DimenRes paddingEndResId: Int = 0,
    @DimenRes paddingStartResId: Int = 0,
    isLastDrawn: Boolean = false,
    isCentered: Boolean = false
) = DividerItemDecoration(
    ContextCompat.getDrawable(context, dividerResId),
    orientation,
    if (paddingEndResId != 0) context.resources.getDimensionPixelOffset(paddingEndResId) else 0,
    if (paddingStartResId != 0) context.resources.getDimensionPixelOffset(paddingStartResId) else 0,
    isLastDrawn,
    isCentered
)

fun DividerItemDecoration(
    context: Context,
    @DrawableRes dividerResId: Int,
    orientation: Int = LinearLayout.HORIZONTAL,
    @DimenRes paddingResId: Int = 0,
    isLastDrawn: Boolean = false,
    isCentered: Boolean = false
) = DividerItemDecoration(
    context,
    dividerResId,
    orientation,
    paddingResId,
    paddingResId,
    isLastDrawn,
    isCentered
)