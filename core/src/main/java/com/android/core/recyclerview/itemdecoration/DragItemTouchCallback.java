package com.android.core.recyclerview.itemdecoration;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import com.android.core.recyclerview.adapter.BaseAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DragItemTouchCallback<T> extends ItemTouchHelper.SimpleCallback {

    private final BaseAdapter<T> adapter;

    public DragItemTouchCallback(BaseAdapter<T> adapter) {
        super(ItemTouchHelper.UP | ItemTouchHelper.DOWN, 0);
        this.adapter = adapter;
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
        List<T> newList = new ArrayList<>(adapter.getCurrentList());
        int fromPosition = viewHolder.getAdapterPosition();
        int toPosition = target.getAdapterPosition();
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(newList, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(newList, i, i - 1);
            }
        }

        adapter.submitList(newList);
        return true;
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return false;
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {}
}