package com.android.core.recyclerview.itemdecoration;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class VerticalSpanDecoration extends RecyclerView.ItemDecoration {

    private final int spanCount;

    public VerticalSpanDecoration(int spanCount) {
        if (spanCount <= 0) {
            throw new IllegalArgumentException("Span count has to be positive");
        }
        this.spanCount = spanCount;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = parent.getHeight() / spanCount;
        view.setLayoutParams(layoutParams);
        super.getItemOffsets(outRect, view, parent, state);
    }
}