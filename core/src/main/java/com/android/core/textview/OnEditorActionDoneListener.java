package com.android.core.textview;

import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

public interface OnEditorActionDoneListener extends TextView.OnEditorActionListener {

    @Override
    default boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            onDone();
            return true;
        }
        return false;
    }

    void onDone();
}