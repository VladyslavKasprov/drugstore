package com.drugstore.database.di;

import android.app.Application;
import dagger.BindsInstance;
import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(modules = {
        DatabaseModule.class
})
public interface DatabaseComponent {

    @Component.Factory
    interface Factory {
        DatabaseComponent create(@BindsInstance Application application);
    }
}