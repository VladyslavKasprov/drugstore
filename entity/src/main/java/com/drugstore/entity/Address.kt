package com.drugstore.entity

data class Address(
    var name: String,
    var firstName: String,
    var lastName: String,
    var address: String,
    var companyName: String?,
    var postcode: Int,
    var city: String,
    var country: String,
    var phoneNumber: String
)