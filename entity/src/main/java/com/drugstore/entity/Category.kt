package com.drugstore.entity

import java.io.Serializable

data class Category(
    var name: String,
    var description: String,
    var imageUri: String
): Serializable