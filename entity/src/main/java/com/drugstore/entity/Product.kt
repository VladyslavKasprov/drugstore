package com.drugstore.entity

import java.io.Serializable

data class Product(
    var name: String,
    var price: Double,
    var imageUris: List<String>
): Serializable