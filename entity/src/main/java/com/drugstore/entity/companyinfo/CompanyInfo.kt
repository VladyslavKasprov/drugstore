package com.drugstore.entity.companyinfo

sealed class CompanyInfo

data class CompanyAddress(var address: String): CompanyInfo()

data class CompanyEmail(var email: String): CompanyInfo()

data class CompanyPhone(var phoneNumber: String): CompanyInfo()

data class CompanySchedule(var schedule: String): CompanyInfo()