package com.drugstore.repository.di;

import android.app.Application;
import com.drugstore.database.di.DaggerDatabaseComponent;
import com.drugstore.database.di.DatabaseComponent;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

@Module
abstract class DatabaseModule {

    @Provides
    @Singleton
    static DatabaseComponent databaseComponent(Application application) {
        return DaggerDatabaseComponent.factory().create(application);
    }
}