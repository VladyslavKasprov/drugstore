package com.drugstore.repository.di;

import android.app.Application;
import dagger.BindsInstance;
import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(modules = {
        WebserviceModule.class,
        DatabaseModule.class
})
public interface RepositoryComponent {

    @Component.Factory
    interface Factory {
        RepositoryComponent create(@BindsInstance Application application);
    }
}