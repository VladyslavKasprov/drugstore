package com.drugstore.repository.di;

import com.drugstore.webservice.di.DaggerWebserviceComponent;
import com.drugstore.webservice.di.WebserviceComponent;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

@Module
abstract class WebserviceModule {

    @Provides
    @Singleton
    static WebserviceComponent webserviceComponent() {
        return DaggerWebserviceComponent.create();
    }
}