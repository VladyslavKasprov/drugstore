package com.drugstore.webservice.di;

import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(modules = {
        WebserviceModule.class
})
public interface WebserviceComponent {

    @Component.Factory
    interface Factory {
        WebserviceComponent create();
    }
}